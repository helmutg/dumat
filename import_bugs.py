#!/usr/bin/python3
# SPDX-License-Identifier: GPL-3.0-or-later

import argparse

import debianbts

from common import (
    add_database_argument,
    connect_database,
    flip_dict,
    the,
    transaction,
    Version,
)


usertags = {
    "fileconflict": "debian-qa@lists.debian.org",
    "dep17p1": "helmutg@debian.org",
    "dep17p2": "helmutg@debian.org",
    "dep17p3": "helmutg@debian.org",
    "dep17p6": "helmutg@debian.org",
    "dep17p7": "helmutg@debian.org",
}


def main() -> None:
    parser = argparse.ArgumentParser()
    add_database_argument(parser)
    args = parser.parse_args()
    with connect_database(args.database) as db, transaction(db) as cur:
        cur.execute("DELETE FROM bug;")
        for email, tags in flip_dict(usertags).items():
            bugmap = debianbts.get_usertag(email, sorted(tags))
            for tag, bugs in bugmap.items():
                for bug in debianbts.get_status(bugs):
                    sources = set()
                    versions = {}
                    for ver in bug.found_versions:
                        try:
                            source, ver = ver.split("/")
                        except ValueError:
                            pass
                        else:
                            sources.add(source)
                        versions[Version(ver)] = False
                    for ver in bug.fixed_versions:
                        try:
                            source, ver = ver.split("/")
                        except ValueError:
                            pass
                        else:
                            sources.add(source)
                        versions[Version(ver)] = True
                    if bug.source and "," not in bug.source:
                        if not sources:
                            # Sometimes all found/fixed versions lack a
                            # package.
                            sources.add(bug.source)
                        elif bug.package in sources:
                            # Sometimes, the found/fixed package is a binary
                            # rather than a source package.
                            sources.remove(bug.package)
                            sources.add(bug.source)
                    if len(sources) == 1:
                        source = the(sources)
                    else:
                        source = None
                    cur.execute(
                        """
INSERT INTO bug (id, category, source) VALUES (?, ?, ?);
""",
                        (bug.bug_num, tag, source),
                    )
                    cur.executemany(
                        """
INSERT INTO bug_version (id, fixed, version) VALUES (?, ?, ?);
""",
                        (
                            (bug.bug_num, fixed, ver)
                            for ver, fixed in versions.items()
                        ),
                    )
                    affects = set(
                        pkg
                        for pkg in bug.package.split(",")
                        if not pkg.startswith("src:")
                    )
                    affects.update(
                        pkg
                        for pkg in bug.affects
                        if not pkg.startswith("src:")
                    )
                    cur.executemany(
                        """
INSERT INTO bug_affects (bid, package) VALUES (?, ?);
""",
                        ((bug.bug_num, pkg) for pkg in affects),
                    )


if __name__ == "__main__":
    main()
