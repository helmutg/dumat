#!/usr/bin/python3
# SPDX-License-Identifier: GPL-3.0-or-later

import argparse
import contextlib
import datetime
import functools
import hashlib
import logging
import lzma
import multiprocessing
import os
import pathlib
import random
import re
import subprocess
import sys
import tarfile
import time
import typing

import debian.deb822
import debian.debfile
import requests

from dedup.compression import Decompressor
from dedup.hashing import HashlibLike, HashedStream
from dedup.debpkg import DebExtractor

from common import (
    add_database_argument,
    add_parallel_argument,
    connect_database,
    dealias_path,
    existing_path,
    the,
    transaction,
    Version,
)


logger = logging.getLogger("import_mirror")


class GPGV:
    def __init__(
        self,
        paths: typing.Iterable[pathlib.Path] = (
            pathlib.Path("/etc/apt/trusted.gpg"),
            pathlib.Path("/etc/apt/trusted.gpg.d"),
        ),
    ) -> None:
        self.keyrings: list[pathlib.Path] = []
        for path in paths:
            if path.is_dir():
                for entry in path.iterdir():
                    if os.access(entry, os.R_OK):
                        self.keyrings.append(entry)
            elif os.access(path, os.R_OK):
                self.keyrings.append(path)

    def verify(self, content: bytes) -> bytes:
        cmdline = ["gpgv", "--quiet", "--weak-digest", "SHA1", "--output", "-"]
        for keyring in self.keyrings:
            cmdline.extend(("--keyring", str(keyring.absolute())))
        with subprocess.Popen(
            cmdline,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        ) as proc:
            stdout, stderr = proc.communicate(content)
            if proc.wait() != 0:
                raise ValueError("signature verififcation failed: %r" % stderr)
        return stdout


ByteIterable = typing.Iterable[bytes]


def yield_lines(iterable: ByteIterable) -> ByteIterable:
    """Converts an arbitrary bytes iterable into an iterable that yields whole
    lines. The final byte of each returned value (except possibly the last one)
    is a newline or carriage return character. The concatenation of the input
    iterable equals the concatenation of the output iterable."""
    buff = b""
    for data in iterable:
        if data:
            buff += data
            parts = buff.splitlines(True)
            buff = parts.pop()
            yield from parts
    if buff:
        yield buff


def parse_deb822(iterable: ByteIterable) -> typing.Iterator[dict[str, str]]:
    """Parse an iterable of bytes formatted as Deb822 into an iterable of
    str-dicts.
    This raises a UnicodeDecodeError in case the input is not valid utf8 and
    it raises a ValueError in case the input does not parse correctly.
    """
    mapping: dict[str, str] = {}
    key = None
    value = None
    for bline in yield_lines(iterable):
        line = bline.decode("utf8")
        if line == "\n":
            if key is not None:
                mapping[key] = value.strip()
                key = None
            yield mapping
            mapping = {}
        elif key and line.startswith((" ", "\t")):
            value += line
        else:
            if key is not None:
                mapping[key] = value.strip()
            try:
                key, value = line.split(":", 1)
            except ValueError:
                raise ValueError("invalid input line %r" % line)
    if key is not None:
        assert value is not None
        mapping[key] = value.strip()
    if mapping:
        yield mapping


def parse_date_release(s: str) -> datetime.datetime:
    return datetime.datetime.strptime(s, "%a, %d %b %Y %H:%M:%S %Z")


HASH_FUNC = "SHA256"


@contextlib.contextmanager
def download(url: str, timeout: int = 300) -> typing.Iterator[typing.BinaryIO]:
    if url.startswith("file://"):
        with open(url[7:], "rb") as filelike:
            yield filelike
    else:
        try:
            with requests.get(url, timeout=timeout, stream=True) as response:
                response.raise_for_status()
                yield response.raw
        except requests.exceptions.ConnectionError as err:
            logger.warning("retrying download of %s: %s", url, err)
            # Retry a download once
            time.sleep(1)
            with requests.get(url, timeout=timeout, stream=True) as response:
                response.raise_for_status()
                yield response.raw


def update_release(
    database: str, args: tuple[int, str, str, str, str]
) -> None:
    """Retrieve an InRelease file and update the components and release
    tables.
    """
    rid, suite, mirror, keyring, last_updated = args
    with download(f"{mirror}/dists/{suite}/InRelease") as fobj:
        verifier = GPGV((pathlib.Path(keyring),)) if keyring else GPGV()
        # Impose a maximum release size of 4MB.
        inrelease = verifier.verify(fobj.read(1 << 22))
    (info,) = list(parse_deb822([inrelease]))
    if info["Acquire-By-Hash"] != "yes":
        raise ValueError(f"{mirror} {suite} does not support by-hash")
    updated = parse_date_release(info["Date"])
    if (
        last_updated is not None and
        updated < datetime.datetime.fromisoformat(last_updated)
    ):
        return

    files = {}
    for line in info[HASH_FUNC].splitlines():
        parts = line.split()
        if not parts:
            continue
        if len(parts) != 3:
            raise ValueError("invalid %s line %r" % (HASH_FUNC, line))
        files[parts[2]] = parts[0]

    with connect_database(database) as db, transaction(db) as cur:
        cur.execute(
            """
SELECT id, architecture, component, hash FROM component WHERE rid = ?;
""",
            (rid,),
        )
        for cid, arch, comp, hashv in cur.fetchall():
            listname = f"{comp}/binary-{arch}/Packages.xz"
            if not listname in files:
                raise ValueError(f"could not find {comp}/{arch} in {suite}")
            if hashv == files[listname]:
                continue
            cur.execute(
                "UPDATE component SET hash = ?, unprocessed = 1 WHERE id = ?;",
                (files[listname], cid),
            )
        cur.execute(
            "UPDATE release SET updated = ? WHERE id = ?;", (updated, rid)
        )


def iter_file(fobj: typing.BinaryIO, chunksize: int = 65536) -> ByteIterable:
    while data := fobj.read(chunksize):
        yield data


class HashSumMismatch(Exception):
    pass


def hash_check(
    iterable: ByteIterable, hashobj: HashlibLike, expected_digest: str
) -> ByteIterable:
    """Wraps an iterable that yields bytes. It doesn't modify the sequence,
    but on the final element it verifies that the concatenation of bytes
    yields an expected digest value. Upon failure, the final next() results in
    a HashSumMismatch rather than StopIteration.
    """
    for data in iterable:
        hashobj.update(data)
        yield data
    if hashobj.hexdigest() != expected_digest:
        raise HashSumMismatch()


def decompress_stream(
    iterable: ByteIterable, decompressor: Decompressor
) -> ByteIterable:
    """Decompress an iterable of bytes using the given decompressor into
    another (decompressed) iterable of bytes. The decompressor can be a
    bz2.BZ2Decompressor or lzma.LZMADecompressor instance."""
    yield from map(decompressor.decompress, iterable)
    if hasattr(decompressor, "flush"):
        yield decompressor.flush()


def parse_source_field(
    value: typing.Optional[str],
) -> tuple[typing.Optional[str], typing.Optional[str]]:
    """Decompose a possible Source field value into the source package name
    and the source version if available.
    """
    if value is None:
        return (None, None)
    match = re.match(r"^(\S+)\s+\((\S+)\)$", value)
    if not match:
        return (value, None)
    source, sourceversion = match.groups()
    return (source, sourceversion)


def update_component(
    database: str, args: tuple[int, str, str, str, str, str]
) -> None:
    """Retrieve an individual package list for one component update the
    relevant tables.
    The flag that this list needs an update is cleared from the component
    table. The package table is updated with the relevant version and a flag
    to actually update the package. The package_membership table is updated.
    """
    cid, arch, comp, mirror, suite, hashv = args
    url = f"{mirror}/dists/{suite}/{comp}/binary-{arch}/by-hash/{HASH_FUNC}/{hashv}"
    with download(url) as fobj:
        dataiter: ByteIterable = iter_file(fobj)
        dataiter = hash_check(dataiter, hashlib.new(HASH_FUNC), hashv)
        dataiter = decompress_stream(dataiter, lzma.LZMADecompressor())
        pkgs: dict[str, dict[str, str]] = {}
        for package in parse_deb822(dataiter):
            name = package["Package"]
            if (
                name in pkgs
                and Version(package["Version"]) < pkgs[name]["Version"]
            ):
                continue
            pkgs[name] = package

    with connect_database(database) as db, transaction(db) as cur:
        cur.execute(
            """
SELECT name, version, m.pid
    FROM package JOIN package_membership AS m ON package.id = m.pid
    WHERE m.cid = ?;
""",
            (cid,),
        )
        deleteids = set()
        for name, version, pid in cur.fetchall():
            if name not in pkgs or pkgs[name]["Version"] != version:
                deleteids.add(pid)
            else:
                del pkgs[name]
        if deleteids:
            logger.info("deleting %d package associations", len(deleteids))
            cur.executemany(
                "DELETE FROM package_membership WHERE cid = ? AND pid = ?;",
                ((cid, pid) for pid in deleteids),
            )
        addids = {}
        for package in pkgs.values():
            cur.execute(
                """
SELECT id, hash
    FROM package WHERE name = ? AND version = ? AND architecture = ?;
""",
                (
                    package["Package"],
                    package["Version"],
                    package["Architecture"],
                ),
            )
            row = cur.fetchone()
            if row:
                if row[1] != package[HASH_FUNC]:
                    raise ValueError(
                        "package %s differs in content" % package["Package"]
                    )
                addids[row[0]] = package["Filename"]
                continue
            source, sourceversion = parse_source_field(package.get("Source"))
            cur.execute(
                """
INSERT INTO package (name, architecture, version, source, sourceversion, hash, unprocessed, uses_dpkg_divert)
    VALUES (?, ?, ?, ?, ?, ?, 1, NULL);
""",
                (
                    package["Package"],
                    package["Architecture"],
                    package["Version"],
                    source,
                    sourceversion,
                    package[HASH_FUNC],
                ),
            )
            addids[cur.lastrowid] = package["Filename"]
        if addids:
            logger.info("adding %d packages", len(addids))
            cur.executemany(
                """
INSERT INTO package_membership (cid, pid, filename) VALUES (?, ?, ?);
""",
                ((cid, pid, filename) for pid, filename in addids.items()),
            )
        cur.execute(
            "UPDATE component SET unprocessed = false WHERE id = ?;", (cid,)
        )


class ProcessingFinished(Exception):
    pass


class UsrMergeExtractor(DebExtractor):
    def __init__(self) -> None:
        DebExtractor.__init__(self)
        self.uses_dpkg_divert = False
        self.files: dict[tuple[bool, str], str] = {}
        self.relations: list[tuple[str, str, typing.Optional[str]]] = []
        self.triggers: set[str] = set()
        self.multiarch: typing.Optional[str] = None

    def handle_control_info(self, info: debian.deb822.Packages) -> None:
        for kind in (
            "breaks",
            "conflicts",
            "depends",
            "pre-depends",
            "provides",
            "replaces",
        ):
            for relalts in info.relations.get(kind, ()):
                if len(relalts) != 1:
                    if kind not in ("depends", "pre-depends"):
                        raise ValueError("no alternatives allowed")
                    # We only record unconditional dependencies here.
                    continue
                relation = relalts[0]
                version = relation["version"]
                self.relations.append(
                    (
                        kind,
                        relation["name"],
                        " ".join(version) if version else None,
                    )
                )
        self.multiarch = info.get("Multi-Arch")

    def handle_control_member(self, name: str, content: bytes) -> None:
        if name == "preinst":
            if b"dpkg-divert" in content:
                self.uses_dpkg_divert = True
        elif name == "triggers":
            for lineb in content.splitlines():
                line = lineb.split(b"#", 1)[0].decode("utf8")
                if not line:
                    continue
                # unpack might fail with a ValueError
                action, subject = line.split(maxsplit=1)
                if (
                    action in (
                        'interest', 'interest-await', 'interest-noawait'
                    ) and subject.startswith("/")
                ):
                    self.triggers.add(subject[1:])

    def handle_data_tar(self, tarfileobj: tarfile.TarFile) -> None:
        for elem in tarfileobj:
            name = elem.name
            while name.startswith("/"):
                name = name[1:]
            while name.startswith("./"):
                name = name[2:]
            # Sometimes, directory members carry a trailing slash. Normalize.
            while name.endswith("/"):
                assert elem.isdir()
                name = name[:-1]
            judgement = dealias_path(name)
            if judgement is None:
                continue
            if elem.isreg():
                filetype = "f"
            elif elem.isdir():
                filetype = "d"
            elif elem.issym():
                filetype = "l" + elem.linkname
            else:
                filetype = "s"
            self.files[judgement] = filetype
        raise ProcessingFinished()


def update_package(database: str, args: tuple[int, str, str]) -> None:
    """Retrieve and analyze an individual .deb package and update the relevant
    tables.
    The content, dpkgtrigger and relation tables are updated. The package table
    is also updated and may trigger the import of diversions.
    """

    pid, hashv, url = args
    extractor = UsrMergeExtractor()
    with download(url) as fobj:
        hashedfobj = HashedStream(fobj, hashlib.new(HASH_FUNC))
        try:
            extractor.process(hashedfobj)
        except ProcessingFinished:
            pass
        else:
            raise RuntimeError("unexpected termination of extractor")
        hashedfobj.validate(hashv)
    with connect_database(database) as db, transaction(db) as cur:
        cur.execute("DELETE FROM relation WHERE pid = ?;", (pid,))
        cur.execute("DELETE FROM content WHERE pid = ?;", (pid,))
        cur.execute("DELETE FROM dpkgtrigger WHERE pid = ?;", (pid,))
        cur.execute("DELETE FROM diversion WHERE pid = ?;", (pid,))
        cur.executemany(
            """
INSERT INTO relation (pid, kind, other, versionrestriction)
    VALUES (?, ?, ?, ?);
""",
            ((pid,) + relation for relation in extractor.relations),
        )
        cur.executemany(
            """
INSERT INTO content (pid, hasusr, filename, filetype) VALUES (?, ?, ?, ?);
""",
            (
                (pid, hasusr, filename, filetype)
                for (hasusr, filename), filetype in extractor.files.items()
            ),
        )
        cur.executemany(
            "INSERT INTO dpkgtrigger (pid, interest) VALUES (?, ?);",
            ((pid, interest) for interest in extractor.triggers),
        )
        cur.execute(
            """
UPDATE package
    SET unprocessed = unprocessed & ~1 | ?, uses_dpkg_divert = ?, multiarch = ?
    WHERE id = ?;
""",
            (
                2 if extractor.uses_dpkg_divert else 0,
                extractor.uses_dpkg_divert,
                extractor.multiarch,
                pid,
            ),
        )
    logger.info("extracted %s", url.split("/")[-1])


def import_mirror(parallel: int, database: str, sample: int | None) -> None:
    with multiprocessing.Pool(parallel) as pool:
        logger.info("phase 1: release")
        with connect_database(database) as db:
            res = db.execute(
                "SELECT id, suite, mirror, keyring, updated FROM release;"
            ).fetchall()
        for _ in pool.imap_unordered(
            functools.partial(update_release, database), res
        ):
            pass
        logger.info("phase 2: lists")
        with connect_database(database) as db:
            res = db.execute(
                """
SELECT c.id, c.architecture, c.component, r.mirror, r.suite, c.hash
    FROM component AS c JOIN release AS r ON c.rid = r.id
    WHERE c.unprocessed = true;
"""
            ).fetchall()
        for _ in pool.imap_unordered(
            functools.partial(update_component, database), res
        ):
            pass
        logger.info("phase 3: package cleanup")
        with connect_database(database) as db:
            db.execute(
                """
DELETE FROM package WHERE id NOT IN (SELECT pid FROM package_membership);
"""
            )
            logger.info("phase 4: new packages")
            res = [
                row + db.execute(
                    """
SELECT r.mirror || '/' || m.filename
    FROM package_membership AS m
        JOIN component AS c ON m.cid = c.id
        JOIN release AS r ON c.rid = r.id
    WHERE m.pid = ? ORDER BY r.id ASC LIMIT 1;
""",
                    (row[0],),
                ).fetchone()
                for row in db.execute(
                    "SELECT id, hash FROM package WHERE unprocessed & 1 = 1;"
                )
            ]
        if sample is not None and sample < len(res):
            res = random.sample(res, sample)
        for _ in pool.imap_unordered(
            functools.partial(update_package, database), res
        ):
            pass


def import_changes(
    parallel: int, database: str, changespath: pathlib.Path
) -> None:
    with changespath.open() as file:
        changes = debian.deb822.Changes(file)
    with connect_database(database) as db, transaction(db) as cur:
        (rid,) = the(
            cur.execute(
                "SELECT id FROM release WHERE suite = ?",
                (changes["Distribution"],),
            )
        )

        package_updates = []
        for obj in changes["Checksums-" + HASH_FUNC]:
            if not obj["name"].endswith(".deb"):
                continue
            filename = changespath.parent / obj["name"]
            debf = debian.debfile.DebFile(filename)
            debc = debf.debcontrol()
            if debc["Package"].endswith("-dbgsym"):
                continue

            component = "main"
            if "/" in debc["Section"]:
                component = debc["Section"].split("/")[0]
            source, sourceversion = parse_source_field(debc.get("Source"))
            cur.execute(
                """
DELETE FROM package_membership
    WHERE pid IN (SELECT id FROM package WHERE name = ? AND version = ?);
""",
                (debc["Package"], debc["Version"]),
            )
            cur.execute(
                """
DELETE FROM package_membership
    WHERE cid IN (SELECT id FROM component WHERE rid = ?)
        AND pid IN (SELECT id FROM package WHERE name = ?);
""",
                (rid, debc["Package"]),
            )
            cur.execute(
                """
DELETE FROM package WHERE id NOT IN (SELECT pid FROM package_membership);
"""
            )
            cur.execute(
                """
INSERT INTO package (name, architecture, version, source, sourceversion, hash, unprocessed, uses_dpkg_divert)
    VALUES (?, ?, ?, ?, ?, ?, 1, NULL);
""",
                (
                    debc["Package"],
                    debc["Architecture"],
                    debc["Version"],
                    source,
                    sourceversion,
                    obj[HASH_FUNC.lower()],
                ),
            )
            pid = cur.lastrowid
            cur.execute(
                """
INSERT INTO package_membership (cid, pid, filename)
    VALUES ((SELECT id FROM component WHERE rid = ? AND component = ?), ?, ?);
""",
                (rid, component, pid, filename.name),
            )
            package_updates.append(
                (pid, obj[HASH_FUNC.lower()], "file://" + str(filename))
            )
    with multiprocessing.Pool(parallel) as pool:
        for _ in pool.imap_unordered(
            functools.partial(update_package, database), package_updates
        ):
            pass


def main() -> None:
    parser = argparse.ArgumentParser()
    add_database_argument(parser)
    add_parallel_argument(parser, "200%")
    parser.add_argument(
        "--changes",
        action="store",
        metavar="PATH",
        default=None,
        type=existing_path,
        help="import packages from a .changes file rather than a mirror",
    )
    parser.add_argument(
        "--sample",
        action="store",
        metavar="COUNT",
        default=None,
        type=int,
        help="import a random sample of COUNT packages",
    )
    args = parser.parse_args()
    logger.addHandler(logging.StreamHandler(sys.stdout))
    logger.setLevel(logging.INFO)
    if args.changes is None:
        import_mirror(args.parallel, args.database, args.sample)
    else:
        import_changes(args.parallel, args.database, args.changes)


if __name__ == "__main__":
    main()
