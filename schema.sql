-- SPDX-License-Identifier: GPL-3.0-or-later

CREATE TABLE release (
	id INTEGER PRIMARY KEY,
	suite TEXT NOT NULL,
	mirror TEXT NOT NULL,
	keyring TEXT DEFAULT NULL,
	baserid INTEGER DEFAULT NULL REFERENCES release(id) ON DELETE RESTRICT,
	upgradesequence INTEGER NOT NULL,
	updated TIMESTAMP DEFAULT NULL
);

CREATE TABLE component (
	id INTEGER PRIMARY KEY,
	rid INTEGER NOT NULL REFERENCES release(id) ON DELETE CASCADE,
	architecture TEXT NOT NULL,
	component TEXT NOT NULL,
	hash TEXT,
	unprocessed BOOL
);

CREATE TABLE package (
	id INTEGER PRIMARY KEY,
	name TEXT NOT NULL,
	architecture TEXT NOT NULL,
	version TEXT NOT NULL,
	source TEXT,
	sourceversion TEXT,
	multiarch TEXT,
	hash TEXT NOT NULL,
	unprocessed INTEGER,
	uses_dpkg_divert BOOLEAN
);

-- unprocessed is a bitmap
-- 0 => all tasks are done
-- 1 => should download package and analyze it
-- 2 => diversions detected, capture diversions


CREATE TABLE package_membership (
	pid INTEGER NOT NULL REFERENCES package(id) ON DELETE RESTRICT,
	cid INTEGER NOT NULL REFERENCES component(id) ON DELETE CASCADE,
	filename TEXT NOT NULL
);

CREATE TABLE relation (
	pid INTEGER NOT NULL REFERENCES package(id) ON DELETE CASCADE,
	kind TEXT NOT NULL CHECK (kind IN ('breaks', 'conflicts', 'depends', 'pre-depends', 'provides', 'replaces')),
	other TEXT NOT NULL,
	versionrestriction TEXT
);

CREATE TABLE content (
	pid INTEGER NOT NULL REFERENCES package(id) ON DELETE CASCADE,
	hasusr BOOLEAN NOT NULL,
	filename TEXT NOT NULL,
	filetype TEXT NOT NULL,
	UNIQUE (pid, filename, hasusr)
);

CREATE TABLE diversion (
	pid INTEGER NOT NULL REFERENCES package(id) ON DELETE CASCADE,
	diverter TEXT NOT NULL,
	filename TEXT NOT NULL,
	target TEXT NOT NULL,
	upgrade BOOLEAN NOT NULL
);

CREATE TABLE dpkgtrigger (
	pid INTEGER NOT NULL REFERENCES package(id) ON DELETE CASCADE,
	interest TEXT NOT NULL
);

CREATE TABLE bug (
	id INTEGER PRIMARY KEY,
	category TEXT NOT NULL,
	source TEXT
);

CREATE TABLE bug_version (
	id INTEGER NOT NULL REFERENCES bug(id) ON DELETE CASCADE,
	fixed BOOLEAN NOT NULL,
	version TEXT NOT NULL
);

CREATE TABLE bug_affects (
	bid INTEGER NOT NULL REFERENCES bug(id) ON DELETE CASCADE,
	package TEXT NOT NULL
);

INSERT INTO release (id, suite, mirror, baserid, keyring, upgradesequence) VALUES
	(1, 'bullseye', 'http://deb.debian.org/debian', NULL, '/usr/share/keyrings/debian-archive-keyring.gpg', 11),
	(2, 'bullseye-security', 'http://deb.debian.org/debian-security', 1, '/usr/share/keyrings/debian-archive-keyring.gpg', 11),
	(3, 'bullseye-proposed-updates', 'http://deb.debian.org/debian', 1, '/usr/share/keyrings/debian-archive-keyring.gpg', 11),
	(4, 'bullseye-updates', 'http://deb.debian.org/debian', 1, '/usr/share/keyrings/debian-archive-keyring.gpg', 11),
	(5, 'bullseye-backports', 'http://deb.debian.org/debian', 1, '/usr/share/keyrings/debian-archive-keyring.gpg', 11),
	(6, 'bookworm', 'http://deb.debian.org/debian', NULL, '/usr/share/keyrings/debian-archive-keyring.gpg', 12),
	(7, 'bookworm-security', 'http://deb.debian.org/debian-security', 6, '/usr/share/keyrings/debian-archive-keyring.gpg', 12),
	(8, 'bookworm-proposed-updates', 'http://deb.debian.org/debian', 6, '/usr/share/keyrings/debian-archive-keyring.gpg', 12),
	(9, 'bookworm-updates', 'http://deb.debian.org/debian', 6, '/usr/share/keyrings/debian-archive-keyring.gpg', 12),
	(10, 'bookworm-backports', 'http://deb.debian.org/debian', 6, '/usr/share/keyrings/debian-archive-keyring.gpg', 12),
	(11, 'trixie', 'http://deb.debian.org/debian', NULL, '/usr/share/keyrings/debian-archive-keyring.gpg', 13),
	(13, 'trixie-proposed-updates', 'http://deb.debian.org/debian', 11, '/usr/share/keyrings/debian-archive-keyring.gpg', 13),
	(14, 'trixie-updates', 'http://deb.debian.org/debian', 11, '/usr/share/keyrings/debian-archive-keyring.gpg', 13),
	(15, 'trixie-backports', 'http://deb.debian.org/debian', 11, '/usr/share/keyrings/debian-archive-keyring.gpg', 13),
	(16, 'unstable', 'http://deb.debian.org/debian', NULL, '/usr/share/keyrings/debian-archive-keyring.gpg', 13),
	(17, 'experimental', 'http://deb.debian.org/debian', 16, '/usr/share/keyrings/debian-archive-keyring.gpg', 13);


INSERT INTO component (rid, architecture, component) VALUES
	(1, 'amd64', 'main'),
	(1, 'amd64', 'contrib'),
	(1, 'amd64', 'non-free'),
	(2, 'amd64', 'main'),
	(2, 'amd64', 'contrib'),
	(2, 'amd64', 'non-free'),
	(3, 'amd64', 'main'),
	(3, 'amd64', 'contrib'),
	(3, 'amd64', 'non-free'),
	(4, 'amd64', 'main'),
	(4, 'amd64', 'contrib'),
	(4, 'amd64', 'non-free'),
	(5, 'amd64', 'main'),
	(5, 'amd64', 'contrib'),
	(5, 'amd64', 'non-free'),
	(6, 'amd64', 'main'),
	(6, 'amd64', 'contrib'),
	(6, 'amd64', 'non-free'),
	(6, 'amd64', 'non-free-firmware'),
	(7, 'amd64', 'main'),
	(7, 'amd64', 'contrib'),
	(7, 'amd64', 'non-free'),
	(7, 'amd64', 'non-free-firmware'),
	(8, 'amd64', 'main'),
	(8, 'amd64', 'contrib'),
	(8, 'amd64', 'non-free'),
	(8, 'amd64', 'non-free-firmware'),
	(9, 'amd64', 'main'),
	(9, 'amd64', 'contrib'),
	(9, 'amd64', 'non-free'),
	(9, 'amd64', 'non-free-firmware'),
	(10, 'amd64', 'main'),
	(10, 'amd64', 'contrib'),
	(10, 'amd64', 'non-free'),
	(10, 'amd64', 'non-free-firmware'),
	(11, 'amd64', 'main'),
	(11, 'amd64', 'contrib'),
	(11, 'amd64', 'non-free'),
	(11, 'amd64', 'non-free-firmware'),
	(13, 'amd64', 'main'),
	(13, 'amd64', 'contrib'),
	(13, 'amd64', 'non-free'),
	(13, 'amd64', 'non-free-firmware'),
	(14, 'amd64', 'main'),
	(14, 'amd64', 'contrib'),
	(14, 'amd64', 'non-free'),
	(14, 'amd64', 'non-free-firmware'),
	(15, 'amd64', 'main'),
	(15, 'amd64', 'contrib'),
	(15, 'amd64', 'non-free'),
	(15, 'amd64', 'non-free-firmware'),
	(16, 'amd64', 'main'),
	(16, 'amd64', 'contrib'),
	(16, 'amd64', 'non-free'),
	(16, 'amd64', 'non-free-firmware'),
	(17, 'amd64', 'main'),
	(17, 'amd64', 'contrib'),
	(17, 'amd64', 'non-free'),
	(17, 'amd64', 'non-free-firmware');

CREATE INDEX package_name_version_index ON package(name, version);
CREATE INDEX package_membership_pid_index ON package_membership(pid);
CREATE INDEX relation_pid_index ON relation(pid);
CREATE INDEX content_filename_index ON content(filename);

PRAGMA journal_mode = 'wal';
