#!/usr/bin/python3
# SPDX-License-Identifier: GPL-3.0-or-later

import argparse
import contextlib
import dataclasses
import itertools
import operator
import os.path
import sqlite3
import sys
import typing

import yaml

from common import (
    add_database_argument, connect_database, dealias_path, the, Version
)


op_map = {
    "<<": operator.lt,
    "<=": operator.le,
    "=": operator.eq,
    ">=": operator.ge,
    ">>": operator.gt,
}


what_to_usertag = {
    "ineffective diversion": "dep17p3",
    "ineffectively diverted": "dep17p3",
    "ineffective replaces": "dep17p1",
    "ineffective trigger interest": "dep17p2",
    "loss of empty directory": "dep17p6",
    "loss of m-a:same shared file": "dep17p7",
    "moved from canonical to aliased": None,
    "risky diversion": "dep17p3",
    "risky empty directory": "dep17p6",
    "risky m-a:same shared file": "dep17p7",
    "risky shared file previously m-a:same": "dep17p7",
    "risky replaces": "dep17p1",
    "risky trigger interest": "dep17p2",
    "undeclared file conflict": "fileconflict",
}


multiarch_subdirs = {
    "amd64": "x86_64-linux-gnu",
    "arm64": "aarch64-linux-gnu",
    "armel": "arm-linux-gnueabi",
    "armhf": "arm-linux-gnueabihf",
    "i386": "i386-linux-gnu",
    "mips64el": "mips64el-linux-gnuabi64",
    "ppc64el": "powerpc64le-linux-gnu",
    "riscv64": "riscv64-linux-gnu",
    "s390x": "s390x-linux-gnu",
}


dynamic_loaders = {
    "amd64": {"lib64/ld-linux-x86-64.so.2", "lib/ld-musl-x86_64.so.1"},
    "arm64": {"lib/ld-linux-aarch64.so.1", "lib/ld-musl-aarch64.so.1"},
    "armel": {"lib/ld-linux.so.3", "lib/ld-musl-arm.so.1"},
    "armhf": {"lib/ld-linux-armhf.so.3", "lib/ld-musl-armhf.so.1"},
    "i386": {"lib/ld-linux.so.2", "lib/ld-musl-i386.so.1"},
    "mips64el": {"lib64/ld.so.1", "lib/ld-musl-mips64el.so.1"},
    "ppc64el": {"lib64/ld64.so.2", "lib/ld-musl-powerpc64le.so.1"},
    "riscv64": {"lib/ld-linux-riscv64-lp64d.so.1"},
    "s390x": {"lib/ld64.so.1", "lib/ld-musl-s390x.so.1"},
}


class Relation:
    """Represent an individual relation to another package with optional
    version restriction. This does not cover architecture qualifiers,
    alternatives, architecture selectors or build profiles."""

    def __init__(self, package: str, versionrestriction: typing.Optional[str]):
        self.package = package
        if versionrestriction is None:
            self.op = None
            self.version = None
        else:
            self.op, version = versionrestriction.split(" ", 1)
            assert self.op in op_map
            self.version = Version(version)

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Relation):
            return False
        return (
            self.package == other.package
            and self.op == other.op
            and self.version == other.version
        )

    def __hash__(self) -> int:
        return hash((self.package, self.op, self.version))

    def __repr__(self) -> str:
        if self.op is None:
            res = ""
        else:
            res = ", %r" % f"{self.op} {self.version}"
        return "%s(%r%s)" % (self.__class__.__name__, self.package, res)

    def __str__(self) -> str:
        if self.op is None:
            return self.package
        return f"{self.package} ({self.op} {self.version})"

    def matches(self, package: str, version: typing.Optional[str]) -> bool:
        """Determine whether this relation matches a given package with
        optional version."""
        if package != self.package:
            return False
        if self.op is None:
            return True
        assert self.version is not None
        if version is None:
            return False
        return bool(op_map[self.op](Version(version), self.version))

    def toprovided(self) -> tuple[str, typing.Optional[Version]]:
        """Assume that this relation comes from a versioned or unversioned
        Provides field and return a pair of package and optionally provided
        version.
        """
        if self.op is None:
            return (self.package, None)
        if self.op != "=":
            raise ValueError("relation has an invalid operator for Provides")
        assert self.version is not None
        return (self.package, self.version)


class AffectedVersions:
    """Model affected versions and provide a containment check."""

    def __init__(self, versions: list[tuple[str, bool]]):
        self.versions = sorted(
            (Version(ver), fixed) for ver, fixed in versions
        )

    @staticmethod
    def from_bug(db: sqlite3.Connection, bugnum: int) -> "AffectedVersions":
        with contextlib.closing(db.cursor()) as cur:
            cur.execute(
                "SELECT version, fixed FROM bug_version WHERE id = ?;",
                (bugnum,),
            )
            return AffectedVersions(cur.fetchall())

    def __contains__(self, version: Version | str) -> bool:
        affected = None
        for ver, isfixed in self.versions:
            if isfixed:
                if version >= ver:
                    affected = False  # marked as fixed
                elif affected is None:
                    affected = True  # assume affected below first fixed
            else:
                if version >= ver:
                    affected = True  # marked as affected
                elif affected is None:
                    affected = False  # assume fixed below first found
        if affected is None:
            return True  # assume affected in the absence of found/fixed
        return affected


@dataclasses.dataclass
class Issue:
    pid: int
    what: str
    files: set[str]
    otherpids: set[int] = dataclasses.field(default_factory=set)

    def merge(self, other: "Issue") -> bool:
        """Attempt to merge another issue into this issue. Return whether
        that was successful.
        """
        for attribute in ("pid", "what", "files"):
            if getattr(self, attribute) != getattr(other, attribute):
                return False
        self.otherpids.update(other.otherpids)
        return True

    def is_downgrade(self, db: sqlite3.Connection) -> bool:
        if not self.otherpids:
            return False
        source, sourceversion = the(
            db.execute(
                """
SELECT ifnull(source, name), ifnull(sourceversion, version)
            FROM package WHERE id = ?;
""",
                (self.pid,),
            )
        )
        for othersource, othersrcversion in db.execute(
            """
SELECT ifnull(source, name), ifnull(sourceversion, version)
    FROM package WHERE id IN (?%s);
"""
            % (", ?" * (len(self.otherpids) - 1)),
            tuple(self.otherpids),
        ):
            if source != othersource:
                break
            if Version(sourceversion) > othersrcversion:
                break
        else:
            # Otherpids that we found refer to the same source package and a
            # lower source version.
            return True
        return False

    def is_supported(self, db: sqlite3.Connection) -> bool:
        def rows_to_set(rows: typing.Iterable[tuple[int]]) -> set[int]:
            return set(the(row) for row in rows)

        torel = rows_to_set(
            db.execute(
                """
SELECT upgradesequence
    FROM release AS r
        JOIN component AS c ON r.id = c.rid
        JOIN package_membership AS m ON c.id = m.cid
    WHERE m.pid = ?;
""",
                (self.pid,),
            )
        )
        fromrel: set[int] = set()
        if self.otherpids:
            fromrel = rows_to_set(
                db.execute(
                    """
    SELECT upgradesequence
        FROM release AS r
            JOIN component AS c ON r.id = c.rid
            JOIN package_membership AS m ON c.id = m.cid
        WHERE m.pid IN (?%s);
    """
                    % (", ?" * (len(self.otherpids) - 1)),
                    tuple(self.otherpids),
                )
            )
        maxrelease: int
        if self.what.startswith("risky "):
            (maxrelease,) = the(
                db.execute("SELECT max(upgradesequence) FROM release;")
            )
            if max(itertools.chain(fromrel, torel)) < maxrelease:
                return False  # Risky are only relevant in development releases
        if (
            self.what in (
                "loss of empty directory", "ineffective trigger interest"
            )
            and the(
                the(
                    db.execute(
                        """
SELECT max(r.id)
    FROM release AS r
        JOIN component AS c ON r.id = c.rid
        JOIN package_membership AS m ON c.id = m.cid
    WHERE m.pid = ?;
""",
                        (self.pid,),
                    )
                )
            ) <= 6  # bookworm
        ):
            # Ignore these two issues for bookworm and older but track
            # regressions for bookworm.
            return False
        if not self.otherpids:
            return True
        if fromrel:
            if min(fromrel) > max(torel):
                return False  # Downgrades not supported
            if max(fromrel) + 1 < min(torel):
                return False  # Skip-upgrades are not supported
        return not self.is_downgrade(db)

    def search_bugs(self, db: sqlite3.Connection) -> typing.Iterator[int]:
        """Search for bugs matching the isssue at hand.
        * The bug category must match exactly.
        * If a bug has source/found/fixed information, it must match one of
          the packages involved in the issue.
        * If the issue mentions packages built from a different source package,
          at least one of them must be affected by the bug.
        * Otherwise the bug source must match the issue package.
        """
        category = what_to_usertag.get(self.what)
        if category is None:
            return
        packages = [
            the(
                db.execute(
                    """
SELECT name, ifnull(source, name), ifnull(sourceversion, version)
    FROM package WHERE id = ?;
""",
                    (pid,),
                )
            )
            for pid in (self.pid, *self.otherpids)
        ]
        assert None not in packages

        for bug, bugsource in db.execute(
            """
SELECT b.id, b.source
    FROM bug AS b JOIN bug_affects AS a ON b.id = a.bid
    WHERE b.category = ? AND a.package = ?
    ORDER BY b.id;
""",
            (category, packages[0][0]),
        ):
            affectedversions = AffectedVersions.from_bug(db, bug)
            if bugsource:
                if bugsource == packages[0][1]:
                    # If the bug in question is assigned against the same
                    # source package as self.pid, only check its own version
                    # and not others' versions.
                    if packages[0][2] not in affectedversions:
                        continue
                else:
                    for _, source, sourceversion in packages[1:]:
                        if (
                            source == bugsource
                            and sourceversion in affectedversions
                        ):
                            break
                    else:
                        continue
            others = set(
                name for name, source, _ in packages if source != bugsource
            )
            if not others:
                if bugsource:
                    yield bug
                continue
            affected = set(
                name
                for name, in db.execute(
                    "SELECT package FROM bug_affects WHERE bid = ?;",
                    (bug,),
                )
            )
            if affected == set((packages[0][0],)):
                yield bug
                continue
            intersection = affected.intersection(others)
            if intersection and bugsource or len(intersection) >= 2:
                yield bug


class Package:
    def __init__(
        self,
        name: str,
        version: str,
        architecture: str,
        multiarch: typing.Optional[str],
    ):
        self.name = name
        self.version = Version(version)
        self.architecture = architecture
        self.multiarch = multiarch
        self.relations: dict[str, set[Relation]] = {}

    def add_relation(self, kind: str, relation: Relation) -> None:
        self.relations.setdefault(kind, set()).add(relation)

    def get_relations(self, *kinds: str) -> typing.Iterator[Relation]:
        for kind in kinds:
            yield from self.relations.get(kind, set())

    def get_provided(
        self, withprovides: bool = True
    ) -> set[tuple[str, str | None]]:
        """Compute a sequence of provided (package, version) pairs. Unless
        withprovides is False, declared Provides are considered.
        """
        provides: set[tuple[str, str | None]] = {
            (self.name, self.version),
        }
        if withprovides:
            provides.update(
                relation.toprovided()
                for relation in self.get_relations("provides")
            )
        return provides

    def check_relations(self, kind: str, other: "Package") -> bool:
        """Check whether other is matched by the relations of given kind on
        self. Note that architectures are disregarded and cross-architecture
        relations are considered satisfied.
        """
        return any(
            relation.matches(*provided)
            for relation in self.get_relations(kind)
            for provided in other.get_provided(
                kind in ("breaks", "conflicts", "depends", "pre-depends")
            )
        )

    def check_concurrent_unpack_disallowed(self, other: "Package") -> bool:
        """Check whether the relations of the self package disallow concurrent
        unpack. The primary mechanism here is to declare Conflicts, but a
        Pre-Depends with an unsatisfied version restriction also prohibits
        concurrent unpacks.
        """
        provides = other.get_provided()
        return any(
            relation.matches(*provided)
            for relation in self.get_relations("conflicts")
            for provided in provides
        ) or any(
            provided[0] == relation.package and not relation.matches(*provided)
            for relation in self.get_relations("pre-depends")
            for provided in provides
        )


def actual_path(filename: str, hasusr: bool) -> str:
    """Reconstruct the actual filename from a normalized one and a filetype
    string containing the aliasing information."""
    return "usr/" + filename if hasusr else filename


def is_masame_shared(filename: str, architecture: str) -> bool:
    """Determine whether the given filename probably is shared in a Multi-Arch:
    same package. This is an architecture-dependent heuristic.
    """
    if ("/%s/" % multiarch_subdirs[architecture]) in filename:
        return False
    if filename.startswith("lib/klibc-"):
        return False
    muslsubdir = (
        multiarch_subdirs[architecture]
        .replace("-gnuabi64", "-musl")
        .replace("-gnu", "-musl")
    )
    if filename.startswith("lib/%s" % muslsubdir):
        return False
    return filename not in dynamic_loaders.get(architecture, set())


def diagnose_conflicts(db: sqlite3.Connection) -> typing.Iterator[Issue]:
    """This yields the following problem categories:
     * DEP17-P1: Ineffective and possibly ineffective Replaces.
     * DEP17-P3: Ineffective and possibly ineffective diversions.
     * Undeclared file conflicts.
    """
    # Map (pid1, pid2) to all the conflicting filenames
    conflicts: dict[tuple[int, int], dict[str, tuple[bool, bool]]] = {}
    pids = set()
    for filename, rows in itertools.groupby(
        db.execute(
            """
SELECT filename, pid, hasusr, filetype FROM content ORDER BY filename;
"""
        ),
        operator.itemgetter(0),
    ):
        instances = [row[1:] for row in rows]
        # Ignore unique files
        if len(instances) < 2:
            continue
        # Ignore directory vs directory conflicts
        if all(ft == "d" for _, _, ft in instances):
            continue
        if (
            filename in (
                "bin", "lib", "lib32", "lib64", "libo32", "libx32", "sbin"
            )
            and all(ft[0] in "dl" for _, _, ft in instances)
        ):
            # Ignore symlink vs directory conflicts around the aliasing links.
            # Since base-files is essential and never removed, we dpkg will not
            # touch them. Assume that all such links belong to base-files.
            continue
        for (pid1, hu1, ft1), (pid2, hu2, ft2) in itertools.combinations(
            instances, 2
        ):
            if not (ft1 == "d" and ft2 == "d"):
                conflicts.setdefault((pid1, pid2), {})[filename] = (hu1, hu2)
                pids.add(pid1)
                pids.add(pid2)

    # Once we reach this point, we've finished the expensive part of the
    # computation. The bulk of CPU and RAM has been consumed here, so there
    # is little point in optimizing anything that follows.

    # Map package ids to package metadata
    pidmap = {
        row[0]: Package(*row[1:])
        for row in db.execute(
            "SELECT id, name, version, architecture, multiarch FROM package;"
        )
        if row[0] in pids
    }
    pids.clear()

    # Trim simple package upgrades to another version of the same package.
    for pid1, pid2 in list(conflicts):
        if pid1 == pid2:
            yield Issue(
                what="internal aliasing conflict",
                pid=pid1,
                files=set(conflicts[(pid1, pid2)]),
            )
            del conflicts[(pid1, pid2)]
        elif pidmap[pid1].name == pidmap[pid2].name:
            if pidmap[pid1].architecture == pidmap[pid2].architecture:
                assert pidmap[pid1].version != pidmap[pid2].version
                sign = -1 if pidmap[pid1].version < pidmap[pid2].version else 1
                decanonicalized = set(
                    "usr/" + filename
                    for filename, (hu1, hu2) in conflicts[(pid1, pid2)].items()
                    if int(hu2) - int(hu1) == sign
                )
                if decanonicalized:
                    yield Issue(
                        what="moved from canonical to aliased",
                        pid=pid1 if sign > 0 else pid2,
                        otherpids={pid1 if sign < 0 else pid2},
                        files=decanonicalized,
                    )
                del conflicts[(pid1, pid2)]
            elif (
                pidmap[pid1].multiarch != "same"
                or pidmap[pid2].multiarch != "same"
                or pidmap[pid1].version != pidmap[pid2].version
            ):
                del conflicts[(pid1, pid2)]
            else:
                pids.add(pid1)
                pids.add(pid2)
        else:
            pids.add(pid1)
            pids.add(pid2)

    for pid in set(pidmap).difference(pids):
        del pidmap[pid]
    pids.clear()

    # Gather package relations
    for pid, kind, other, versionrestriction in db.execute(
        "SELECT pid, kind, other, versionrestriction FROM relation;"
    ):
        if pid in pidmap:
            pidmap[pid].add_relation(kind, Relation(other, versionrestriction))

    # Trim declared file conflicts
    for pid1, pid2 in list(conflicts):
        if (
            pidmap[pid1].check_concurrent_unpack_disallowed(pidmap[pid2]) or
            pidmap[pid2].check_concurrent_unpack_disallowed(pidmap[pid1])
        ):
            del conflicts[(pid1, pid2)]
        else:
            pids.add(pid1)
            pids.add(pid2)

    for pid in set(pidmap).difference(pids):
        del pidmap[pid]
    pids.clear()

    # Mirror all file conflicts such that we can deal with them
    # unidirectionally.
    for (pid1, pid2), filemap in list(conflicts.items()):
        conflicts[(pid2, pid1)] = {
            filename: pair[::-1] for filename, pair in filemap.items()
        }

    # Gather use of dpkg-divert
    diversions: dict[int, dict[str, str]] = {
        pid: dict(map(operator.itemgetter(1, 2), rows))
        for pid, rows in itertools.groupby(
            db.execute(
                """
SELECT pid, filename, diverter
    FROM diversion
    WHERE upgrade IS FALSE
    ORDER BY pid;
""",
            ),
            operator.itemgetter(0),
        )
        if pid in pidmap
    }

    for (pid1, pid2), filemap in conflicts.items():
        pkg1 = pidmap[pid1]
        pkg2 = pidmap[pid2]
        if pkg1.check_relations("replaces", pkg2):
            # Account for DEP17 M8
            filemap = {
                filename: (hu1, hu2)
                for filename, (hu1, hu2) in filemap.items()
                if (
                    diversions.get(pid1, {}).get(
                        actual_path(filename, not hu1), pkg2.name
                    )
                    == pkg2.name
                )
            }
            broken_replaces = set(
                actual_path(filename, hu1)
                for filename, (hu1, hu2) in filemap.items()
                if hu1 != hu2
            )
            if broken_replaces:
                yield Issue(
                    what="ineffective replaces",
                    pid=pid1,
                    otherpids={pid2},
                    files=broken_replaces,
                )
                continue
            risky_replaces = set(
                actual_path(filename, hu1)
                for filename, (hu1, hu2) in filemap.items()
                if not (hu1 and hu2)
            )
            if risky_replaces:
                yield Issue(
                    what="risky replaces",
                    pid=pid1,
                    otherpids={pid2},
                    files=risky_replaces,
                )
            continue
        if pkg2.check_relations("replaces", pkg1):
            # We always want an issue around Replaces to show up on the
            # replacing package rather than the replaced package. Thus ignore
            # this mirrored conflict.
            continue

        issues: dict[str, set[str]] = {
            "ineffective diversion": set(),
            "ineffectively diverted": set(),
            "risky diversion": set(),
            "undeclared file conflict": set(),
        }
        for filename, (hu1, hu2) in filemap.items():
            diverted_left = (
                diversions.get(pid1, {}).get(filename, pkg2.name) != pkg2.name,
                diversions.get(
                    pid1, {}
                ).get("usr/" + filename, pkg2.name) != pkg2.name,
            )
            if diverted_left[hu2] and diverted_left[True]:
                continue  # Actually diverted and canonically diverted is good.
            diverted_right = (
                diversions.get(pid2, {}).get(filename, pkg1.name) != pkg1.name,
                diversions.get(
                    pid2, {}
                ).get("usr/" + filename, pkg1.name) != pkg1.name,
            )
            if any(diverted_left) and not diverted_left[hu2]:
                issues["ineffective diversion"].add(actual_path(filename, hu1))
            elif any(diverted_right) and not diverted_right[hu1]:
                issues["ineffectively diverted"].add(
                    actual_path(filename, hu1)
                )
            elif diverted_left[hu2] and not diverted_left[True]:
                issues["risky diversion"].add(actual_path(filename, hu1))
            elif not any(diverted_right):
                issues["undeclared file conflict"].add(
                    actual_path(filename, hu1)
                )
        for what, files in issues.items():
            if files:
                yield Issue(
                    what=what,
                    pid=pid1,
                    otherpids={pid2},
                    files=files,
                )


def diagnose_triggers(db: sqlite3.Connection) -> typing.Iterator[Issue]:
    """DEP17-P2"""
    for (pid, name), rows in itertools.groupby(
        db.execute(
            """
SELECT p.id, p.name, t.interest
    FROM dpkgtrigger AS t JOIN package AS p ON t.pid = p.id
    ORDER BY p.id;
"""
        ),
        operator.itemgetter(0, 1),
    ):
        interests: dict[str, set[bool]] = {}
        for hasusr, filename in filter(
            None, map(dealias_path, map(operator.itemgetter(2), rows))
        ):
            interests.setdefault(filename, set()).add(hasusr)
        aliasedinterest: set[str] = set()
        missinginterest: set[str] = set()
        otherpids: set[int] = set()
        for filename, variants in interests.items():
            if len(variants) > 1:
                continue
            interesthasusr = the(variants)
            if not interesthasusr:
                aliasedinterest.add(filename)
            for (opid,) in db.execute(
                """
SELECT p.id
    FROM content AS c JOIN package AS p ON c.pid = p.id
    WHERE c.filename = ? AND c.hasusr = ? AND p.name != ?;
""",
                (filename, not interesthasusr, name),
            ):
                missinginterest.add(actual_path(filename, interesthasusr))
                aliasedinterest.discard(filename)
                otherpids.add(opid)
        if missinginterest:
            yield Issue(
                what="ineffective trigger interest",
                pid=pid,
                otherpids=otherpids,
                files=missinginterest,
            )
        if aliasedinterest:
            yield Issue(
                what="risky trigger interest",
                pid=pid,
                files=aliasedinterest,
            )


def diagnose_emptydir(db: sqlite3.Connection) -> typing.Iterator[Issue]:
    """DEP17-P6"""
    # The entries of the white list are non-empty in some essential package and
    # therefore never removed.
    whitelist = {"bin", "lib", "sbin"}
    pidmap: dict[int, str] = dict(db.execute("SELECT id, name FROM package;"))
    empty_dirs: dict[str, set[int]] = {}
    inhabitants: dict[str, set[int]] = {}
    for filename, pid, architecture, hasusr, filetype in db.execute(
        """
SELECT c.filename, p.id, p.architecture, c.hasusr, c.filetype
    FROM content AS c JOIN package AS p ON c.pid = p.id
    ORDER BY c.filename ASC;
"""
    ):
        hasusr = bool(hasusr)
        # Regardless of the locale, any filename sorts after any of its
        # prefixes.
        aliasedname = filename
        otherfilename = actual_path(filename, not hasusr)
        filename = actual_path(filename, hasusr)
        if filetype == "d":
            if aliasedname not in whitelist and (
                architecture == "all"
                or aliasedname != "lib/" + multiarch_subdirs[architecture]
            ):
                empty_dirs.setdefault(filename, set()).add(pid)
            inhabitants.setdefault(filename, set()).add(pid)
        # An inhabitant in both canonical and aliased location prevents a
        # directory from being removed.
        for parent in map(os.path.dirname, (filename, otherfilename)):
            assert isinstance(parent, str)
            if parent != "":
                pids = empty_dirs.get(parent)
                if pids:
                    pids.discard(pid)
                    if not pids:
                        del empty_dirs[parent]
    for filename, pids in empty_dirs.items():
        if filename.startswith("usr/"):
            aliased = filename[4:]
        else:
            aliased = "usr/" + filename
        for pid in pids:
            if pid in empty_dirs.get(aliased, set()):
                continue
            otherpids = empty_dirs.get(aliased, set()).copy()
            if inhabitants.get(aliased):
                otherpids.update(
                    pid2
                    for pid2 in inhabitants[aliased]
                    if pidmap[pid] != pidmap[pid2]
                )
            if otherpids:
                yield Issue(
                    what="loss of empty directory",
                    pid=pid,
                    files={filename},
                    otherpids=otherpids,
                )
            elif not filename.startswith("usr/"):
                yield Issue(
                    what="risky empty directory",
                    pid=pid,
                    files={filename},
                    otherpids=set(
                        pid2
                        for pid2 in inhabitants.get(filename, set())
                        if pidmap[pid] != pidmap[pid2]
                    ),
                )


def diagnose_multiarch(db: sqlite3.Connection) -> typing.Iterator[Issue]:
    """DEP17-P7"""
    seen: dict[int, set[str]] = {}
    for (
        pid1,
        version1,
        architecture1,
        multiarch1,
        pid2,
        version2,
        architecture2,
        multiarch2,
    ), rows in itertools.groupby(
        # We look for files such that:
        #  * They're aliased in the first package.
        #  * They're canonical in the second package.
        #  * Neither of them is a directory.
        #  * At least one of the packages is M-A:same.
        #  * M10: The aliased location is not diverted in the second package.
        db.execute(
            """
SELECT pa.id,
        pa.version,
        pa.architecture,
        pa.multiarch,
        pb.id,
        pb.version,
        pb.architecture,
        pb.multiarch,
        ca.filename
    FROM content AS ca
        JOIN package AS pa ON ca.pid = pa.id
        JOIN content AS cb ON ca.filename = cb.filename
        JOIN package AS pb ON cb.pid = pb.id
        LEFT JOIN diversion AS db
            ON db.pid = pb.id
            AND db.filename = ca.filename
            AND db.diverter != pb.name
    WHERE pa.name = pb.name
        AND (pa.multiarch = 'same' OR pb.multiarch = 'same')
        AND ca.hasusr = false
        AND cb.hasusr = true
        AND ca.filetype != 'd'
        AND cb.filetype != 'd'
        AND db.pid IS NULL;
"""
        ),
        operator.itemgetter(0, 1, 2, 3, 4, 5, 6, 7),
    ):
        files = set(
            row[8] for row in rows
            if (
                architecture1 != architecture2
                or is_masame_shared(row[8], architecture1)
            )
        )
        if not files:
            continue

        if Version(version1) < version2:
            oldpid = pid1
            oldmultiarch = multiarch1
            newpid = pid2
        else:
            oldpid = pid2
            oldmultiarch = multiarch2
            newpid = pid1

        if multiarch1 == 'same' and multiarch2 == 'same':
            seen.setdefault(newpid, set()).update(files)
            yield Issue(
                what="loss of m-a:same shared file",
                pid=newpid,
                files=files,
                otherpids=set((oldpid,)),
            )
        elif oldmultiarch == 'same':
            # The loss scenario only happens when both the old and new version
            # are both m-a:same. If only the old one is thus marked, we get:
            # package foo:amd64 (2) with field 'Multi-Arch: no' is not
            # co-installable with foo which has multiple installed instances
            yield Issue(
                what="risky shared file previously m-a:same",
                pid=newpid,
                files=files,
                otherpids=set((oldpid,)),
            )

    for pid, rows in itertools.groupby(
        db.execute(
            """
SELECT p.id, p.architecture, c.filename
    FROM content AS c JOIN package AS p ON c.pid = p.id
    WHERE c.filetype != 'd' AND p.multiarch = 'same' AND hasusr = false
    ORDER BY pid;
"""
        ),
        operator.itemgetter(0),
    ):
        files = set(
            row[2] for row in rows if is_masame_shared(row[2], row[1])
        ).difference(seen.get(pid, set()))
        if files:
            yield Issue(
                what="risky m-a:same shared file",
                pid=pid,
                files=files,
            )


def main() -> None:
    parser = argparse.ArgumentParser()
    add_database_argument(parser)
    available_analyzers = {
        "conflicts": diagnose_conflicts,
        "triggers": diagnose_triggers,
        "emptydir": diagnose_emptydir,
        "multiarch": diagnose_multiarch,
    }
    parser.add_argument(
        "--analyzer",
        choices=available_analyzers.keys(),
        help="run only the specified analyzer",
    )
    args = parser.parse_args()
    with connect_database(args.database) as db:
        # Collect relevant metadata
        pidmap: dict[int, Package] = {
            row[0]: Package(*row[1:])
            for row in db.execute(
                """
SELECT id, name, version, architecture, multiarch FROM package;
                """,
            )
        }
        suites: dict[int, list[int]] = {
            pid: list(map(operator.itemgetter(1), rows))
            for pid, rows in itertools.groupby(
                db.execute(
                    """
SELECT DISTINCT m.pid, c.rid
    FROM package_membership AS m JOIN component AS c ON m.cid = c.id
    ORDER BY m.pid, c.rid ASC;
"""
                ),
                operator.itemgetter(0),
            )
        }
        suitenames: dict[int, str] = dict(
            db.execute("SELECT id, suite FROM release;")
        )

        issuelist: list[Issue] = []
        for issue in itertools.chain.from_iterable(
            analyzer(db)
            for analyzer in (
                available_analyzers.values()
                if args.analyzer is None
                else (available_analyzers[args.analyzer],)
            )
        ):
            # This is O(n^2), but the expected number of issues is
            # small enough that it hopefully doesn't matter.
            for candidate in issuelist:
                if candidate.merge(issue):
                    break
            else:
                issuelist.append(issue)

        issues: typing.Any = {}  # Skip type checking for the output object
        for issue in issuelist:
            if not issue.is_supported(db):
                continue

            package = pidmap[issue.pid]
            bugs = list(issue.search_bugs(db))
            pkgtovertorid: dict[str, dict[str, set[int]]] = {}
            for pid in issue.otherpids:
                pkgtovertorid.setdefault(pidmap[pid].name, {}).setdefault(
                    str(pidmap[pid].version), set()
                ).update(suites[pid])
            pkgtovertosuitenames = {
                name: {
                    ver: "|".join(suitenames[rid] for rid in sorted(rids))
                    for ver, rids in vermap.items()
                }
                for name, vermap in pkgtovertorid.items()
            }

            # Convert to output-oriented structure
            issuedict: dict[str, typing.Any] = {
                "what": issue.what,
                "files": sorted(map("/".__add__, issue.files)),
            }
            if bugs:
                issuedict["bugs"] = bugs
            if pkgtovertosuitenames:
                issuedict["others"] = pkgtovertosuitenames
            inst = issues.setdefault(
                package.name, {}
            ).setdefault(str(package.version), {})
            inst["suites"] = "|".join(
                suitenames[rid] for rid in suites[issue.pid]
            )
            source, sourceversion = the(
                db.execute(
                    "SELECT source, sourceversion FROM package WHERE id = ?;",
                    (issue.pid,),
                )
            )
            if source is not None:
                inst["source"] = source
            if sourceversion is not None:
                inst["sourceversion"] = sourceversion
            inst.setdefault("issues", []).append(issuedict)

    yaml.safe_dump(issues, sys.stdout)


if __name__ == "__main__":
    main()
